// set up a temporary (in memory) database
const Datastore = require('nedb')
const puppies = require('../data/puppies.json')
const users = require('../data/users.json')
const userController = require('../controllers/user')
const LOG = require('../utils/logger.js')

module.exports = (app) => {
  LOG.info('START seeder.')
  const db = {}

  db.puppies = new Datastore()
  db.users = new Datastore()

  db.puppies.loadDatabase()
  db.users.loadDatabase()

  // insert the sample data into our datastore
  db.puppies.insert(puppies)

  // register each user
  users.forEach((user) => {
    userController.newUser(user)
  })

  // initialize app.locals (these objects will be available to our controllers)
  app.locals.puppies = db.puppies.find(puppies)
  LOG.debug(`${app.locals.puppies.query.length} puppies seeded`)

  app.locals.users = db.users.find(users)
  LOG.debug(`${app.locals.users.query.length} users registered`)

  LOG.info('END Seeder. Sample data read and verified.')
}
